using Fusion;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game15Server
{
    /// <summary>
    /// Events handler
    /// </summary>
    [SimulationBehaviour(Modes = SimulationModes.Server)]
    public class EventHandler : SimulationBehaviour
    {
        #region Monobehaviour callbacks
        // Start is called before the first frame update
        void Start()
        {

        }
        #endregion

        #region Public methods
        /// <summary>
        /// Shutting down the server on button click.
        /// </summary>
        public void ShutDownServer()
        {
            try
            {
                // Shut down the runner.
                Runner.Shutdown();
                // Load the Menu scene.
                SceneManager.LoadScene(1);

                
            }
            catch
            {
                Debug.LogError($"Failed to shut down the server");
            }
        }
        #endregion

    }

}

