using Fusion;
using Fusion.Sockets;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;

namespace Game15Server
{
    /// <summary>
    /// Player movement
    /// </summary>
    public unsafe class PlayerMovement : NetworkBehaviour, INetworkRunnerCallbacks
    {
        /// <summary>
        /// Player speed
        /// </summary>
        [Range(0,50)]
        [SerializeField] private float _playerSpeed = 10f;
        /// <summary>
        /// Jump speed
        /// </summary>
        [Range(0, 50)]
        [SerializeField] private float _jumpSpeed = 10;
        [Range(0, 50)]
        [SerializeField] private float _rotationSpeed = 10f;

        /// <summary>
        /// Animation state controller script
        /// </summary>
        [SerializeField] private AnimationStateController _animationStateController;

        /// <summary>
        /// Ground check animation script
        /// </summary>
        [SerializeField] private GroundCheck _groundCheck;


        private bool _isPlayerJumping = false;
        private Vector3 direction;
        private Vector2 rotation;
        private NetworkRigidbody m_Rigidbody;
        private CinemachineController m_CinemachineController;
        private RightPanel m_RightPanel;

        [Networked] public float PlayerSpeed { get; set; }

        [Networked] public Vector2 JoyStickData { get; private set; }

        [Networked] public Vector3 PlayerDirection { get; private set; }

        public Interpolator<float> NetFloatInterpolatorMove;
        public Interpolator<Vector3> NetFloatInterpolatorDirection;
        public Interpolator<Vector2> NetFloatInterpolatorJoystick;

        public NetworkBool CameraAttached;

        #region Monobehaviour callbacks

        private void Awake()
        {

            m_CinemachineController = GameObject.FindGameObjectWithTag("Cinemachine").GetComponent<CinemachineController>();
            Debug.Log($"{nameof(PlayerMovement)} Script : {m_CinemachineController}");
        }

        // Start is called before the first frame update
        void Start()
        {
            
            CameraAttached = false;
        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion

        #region Networkbehaviour callbacks

        public override void Spawned()
        {
            Debug.Log($"{nameof(Spawned)}");
            
            NetFloatInterpolatorMove = GetInterpolator<float>(nameof(PlayerSpeed));

            NetFloatInterpolatorDirection = GetInterpolator<Vector3>(nameof(PlayerDirection));
            NetFloatInterpolatorJoystick = GetInterpolator<Vector2>(nameof(JoyStickData));

            if(m_CinemachineController != null && Object.HasInputAuthority)
            {
                // m_CinemachineController.Player = transform;
                m_CinemachineController.AttachPlayerToCinemachineVirtualCamera(transform);
            }
            else
            {
                Debug.LogError("Cinemachine camera not found.");
            }
            transform.GetComponent<Rigidbody>().isKinematic = false;
            
        }

        public override void FixedUpdateNetwork()
        {
            
            // Debug.Log($"{nameof(PlayerMovement)} : {nameof(FixedUpdateNetwork)}");
            if (GetInput(out NetworkInputData data))
            {

                Move(data);
                //Rotation(data);
                Jumping(data);

            }
        }

        /// <summary>
        /// Post simulation for the physics based systems
        /// </summary>
        public override void Render()
        {
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Player movement.
        /// </summary>
        /// <param name="data"></param>
        void Move(NetworkInputData data) 
        {
            // Joystick values
            var joyStickOut = data.Joystick;
            
            // storing the direction values from the network onto the local variable.
            direction = data.Direction; 

            // Blend tree animation play depending upon the joystick value.
            _animationStateController._Animator.SetFloat(_animationStateController.MoveXhash, joyStickOut.x);
            _animationStateController._Animator.SetFloat(_animationStateController.MoveZhash, joyStickOut.y);

            if (joyStickOut.x < -1 || joyStickOut.x > 1)
            {
                _playerSpeed = 2;
            }
            if (joyStickOut.y > 0 && joyStickOut.y <= 0.45)
            {
                _playerSpeed = 3;
            }
            if (joyStickOut.y > 0.45 && joyStickOut.y <= 0.75)
            {
                _playerSpeed = 4.5f;
            }
            if (joyStickOut.y > 0.75 && joyStickOut.y <= 1)
            {
                _playerSpeed = 6.5f;
            }
            if (joyStickOut.y <= 0)
            {
                _playerSpeed = 2f;
            }

            // Player speed on the network side
            PlayerSpeed = _playerSpeed * Runner.DeltaTime;
            
            // Player speed hash
            _animationStateController._Animator.SetFloat(_animationStateController.SpeedHash, PlayerSpeed);

            // Moving the player
            transform.Translate(direction * NetFloatInterpolatorMove.Value);

            Rotation(data);

        }

        void Rotation(NetworkInputData data)
        {
            rotation = data.PlayerRotation;

            // transform.rotation = Quaternion.Euler(0,rotation.y,0);
            // data = new NetworkInputData();
            data.PlayerRotationValue();
            //Debug.Log($"{}");
            if (rotation.x > 0)
            {
                var val = transform.rotation.y;
                val += 5 ;
                // transform.rotation = Quaternion.Euler(new Vector3(0, val, 0) * Runner.DeltaTime * _rotationSpeed);
                transform.Rotate(Vector3.up * Runner.DeltaTime * _rotationSpeed);
                // Debug.Log("Right Rotation");
            }
            if (rotation.x < 0)
            {
                var val = transform.rotation.y;
                val -= 5;
                // transform.rotation = Quaternion.Euler(new Vector3(0, val, 0) * Runner.DeltaTime * _rotationSpeed);
                transform.Rotate(Vector3.up * Runner.DeltaTime * - _rotationSpeed);
                // Debug.Log("Left Rotation");
            }
        }

        void Jumping(NetworkInputData data)
        {
            // Jump if value is true and Player is grounded.
            if (data.Jumping && !_isPlayerJumping)
            {
                Debug.Log($"{nameof(FixedUpdateNetwork)} : Jump {data.Jumping}");
                _animationStateController._Animator.SetBool(_animationStateController.JumpHash, true);
                transform.GetComponent<Rigidbody>().AddForce(Vector3.up * _jumpSpeed, ForceMode.VelocityChange);
                _isPlayerJumping = true;
                Invoke(nameof(ResetPlayerJumping), 0.5f);
                
            }
        }

        void ResetPlayerJumping()
        {
            Debug.Log($"{nameof(ResetPlayerJumping)} : ");
            _animationStateController._Animator.SetBool(_animationStateController.JumpHash, false);
            _isPlayerJumping = false;
        }
        #endregion

        #region Player Network callbacks
        public void OnPlayerJoined(NetworkRunner runner, PlayerRef player)
        {
            //throw new NotImplementedException();
        }

        public void OnPlayerLeft(NetworkRunner runner, PlayerRef player)
        {
            //throw new NotImplementedException();
        }

        public void OnInput(NetworkRunner runner, NetworkInput input)
        {
            /*NetworkInputData data = new NetworkInputData();

            // Move values reading from the new input system.
            Vector2 move = _controlInputAction.Player.Move.ReadValue<Vector2>();

            // smoothen and damping the move vector2 values.
            _currentInputVector = Vector2.SmoothDamp(_currentInputVector, move, ref _smoothInputVelocity, _smoothInputSpeed);

            // 
            data.direction = new Vector3(_currentInputVector.x, 0, _currentInputVector.y);

            Debug.Log($"{nameof(ServerGameController)} : {nameof(OnInput)} : {data.direction}");

            input.Set(data);*/
            //throw new NotImplementedException();
        }

        public void OnInputMissing(NetworkRunner runner, PlayerRef player, NetworkInput input)
        {
            //throw new NotImplementedException();
        }

        public void OnShutdown(NetworkRunner runner, ShutdownReason shutdownReason)
        {
            //throw new NotImplementedException();
        }

        public void OnConnectedToServer(NetworkRunner runner)
        {
            //throw new NotImplementedException();
        }

        public void OnDisconnectedFromServer(NetworkRunner runner)
        {
            //throw new NotImplementedException();
        }

        public void OnConnectRequest(NetworkRunner runner, NetworkRunnerCallbackArgs.ConnectRequest request, byte[] token)
        {
            //throw new NotImplementedException();
        }

        public void OnConnectFailed(NetworkRunner runner, NetAddress remoteAddress, NetConnectFailedReason reason)
        {
            //throw new NotImplementedException();
        }

        public void OnUserSimulationMessage(NetworkRunner runner, SimulationMessagePtr message)
        {
            //throw new NotImplementedException();
        }

        public void OnSessionListUpdated(NetworkRunner runner, List<SessionInfo> sessionList)
        {
            //throw new NotImplementedException();
        }

        public void OnCustomAuthenticationResponse(NetworkRunner runner, Dictionary<string, object> data)
        {
            //throw new NotImplementedException();
        }

        public void OnHostMigration(NetworkRunner runner, HostMigrationToken hostMigrationToken)
        {
            //throw new NotImplementedException();
        }

        public void OnReliableDataReceived(NetworkRunner runner, PlayerRef player, ArraySegment<byte> data)
        {
            //throw new NotImplementedException();
        }

        public void OnSceneLoadDone(NetworkRunner runner)
        {
            //throw new NotImplementedException();
        }

        public void OnSceneLoadStart(NetworkRunner runner)
        {
            //throw new NotImplementedException();
        }
        #endregion

    }

}
