using Fusion;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game15Server
{
    public class GroundCheck : NetworkBehaviour
    {

        #region Serialize field
        #endregion


        #region Public fields
        /// <summary>
        /// 
        /// </summary>
        [Networked]
        public NetworkBool PlayerGrounded { get; private set; } 
        #endregion

        #region Monobehaviour callbacks
        // Start is called before the first frame update
        void Start()
        {

        }

        private void OnTriggerEnter(Collider other)
        {
            if(other.tag == "Ground")
            {
                PlayerGrounded = true;
                Debug.Log($"{nameof(OnTriggerEnter)} : Player grounded {PlayerGrounded} Tag {other.tag}");
            }
        }

        private void OnTriggerStay(Collider other)
        {
            /*if (other.tag == "Grounded")
            {
                //PlayerGrounded = true;
            }*/
            
            //Debug.Log($"{nameof(OnTriggerStay)} : Player grounded {PlayerGrounded} Tag {other.tag}");
        }

        private void OnTriggerExit(Collider other)
        {
            if(other.tag == "Grounded")
            {
                PlayerGrounded = false;
                Debug.Log($"{nameof(OnTriggerExit)} : Player grounded {PlayerGrounded} Tag {other.tag}");
            }
            
        }

        // Update is called once per frame
        void Update()
        {

        }
        #endregion
    }
}


