using Fusion;
using Fusion.Sockets;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Game15Server
{
    /// <summary>
    /// User Input from local device and store it on the network.
    /// Note : Attach it to the client network runner.
    /// </summary>
    public class NetworkMovement : SimulationBehaviour, INetworkRunnerCallbacks
    {
        [Range(0,50)]
        [SerializeField] private float _smoothInputSpeed = 10f;

        private InputActions _inputActions;
        private Vector2 _currentInputVector;
        private Vector2 _currentRotateVector;
        private Vector2 _smoothInputVelocity;
        private Vector2 _smoothRotateVelocity;
        private RightPanel _rightPanel;

        private bool _jump;

        #region Monobehaviour callbacks
        private void Awake()
        {
            _inputActions = new InputActions();
            _rightPanel = new RightPanel();
        }

        private void OnEnable()
        {
            _inputActions.Enable();
        }

        private void OnDisable()
        {
            _inputActions.Disable();
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            
            
            
        }
        #endregion


        #region Private Fields
        private NetworkInputData PlayerJump(NetworkInputData networkInputData)
        {
            var jump = networkInputData;

            _inputActions.Player.Jump.performed += _ => _jump = true;
            _inputActions.Player.Jump.canceled += _ => _jump = false;

            jump.Jumping = _jump;

            return jump;
        }
        #endregion

        #region INetwork Runner callbacks
        public void OnPlayerJoined(NetworkRunner runner, PlayerRef player)
        {
            //throw new NotImplementedException();
        }

        public void OnPlayerLeft(NetworkRunner runner, PlayerRef player)
        {
            //throw new NotImplementedException();
        }

        public void OnInput(NetworkRunner runner, NetworkInput input)
        {
            var out_ = _inputActions.Player.Move.ReadValue<Vector2>();
            var rotateOut_ = _inputActions.Player.Rotate.ReadValue<Vector2>();
            _currentInputVector = Vector2.SmoothDamp(_currentInputVector, out_, ref _smoothInputVelocity, _smoothInputSpeed);
            _currentRotateVector = Vector2.SmoothDamp(_currentRotateVector, rotateOut_, ref _smoothRotateVelocity, _smoothInputSpeed);

            NetworkInputData networkInputData = new NetworkInputData();

            /*if (out_ != Vector2.zero)
            {

                networkInputData.Joystick = _currentInputVector;
                networkInputData.Direction = new Vector3(_currentInputVector.x, 0, _currentInputVector.y);
                // Debug.Log($"{nameof(NetworkMovement)} : Network Input Data Direction {networkInputData.Direction} Current Input Vector {_currentInputVector} Network Input Data {networkInputData.Joystick}");
                if (rotateOut_ != Vector2.zero)
                {
                    networkInputData.PlayerRotation = _currentRotateVector;
                    // Debug.Log($"{networkInputData.PlayerRotation}");
                }
            }*/

            if (rotateOut_ != Vector2.zero || rotateOut_ == Vector2.zero)
            {
                networkInputData.PlayerRotation = _currentRotateVector;
                if (out_ != Vector2.zero)
                {
                    networkInputData.Joystick = _currentInputVector;
                    networkInputData.Direction = new Vector3(_currentInputVector.x, 0, _currentInputVector.y);
                }
                if (rotateOut_ == Vector2.zero)
                {
                    networkInputData.PlayerRotation = Vector2.zero;
                }
            }

            /*if (rotateOut_ != Vector2.zero)
            {
                networkInputData.PlayerRotation = _currentRotateVector;
                // Debug.Log($"{networkInputData.PlayerRotation}");
            }*/

            else
            {
                networkInputData.Joystick = Vector3.zero;
                // networkInputData.PlayerRotation = Vector3.zero;
            }
            networkInputData = PlayerJump(networkInputData);

            input.Set(networkInputData);

        }

        public void OnInputMissing(NetworkRunner runner, PlayerRef player, NetworkInput input)
        {
            //throw new NotImplementedException();
        }

        public void OnShutdown(NetworkRunner runner, ShutdownReason shutdownReason)
        {
            //throw new NotImplementedException();
        }

        public void OnConnectedToServer(NetworkRunner runner)
        {
            //throw new NotImplementedException();
        }

        public void OnDisconnectedFromServer(NetworkRunner runner)
        {
            //throw new NotImplementedException();
        }

        public void OnConnectRequest(NetworkRunner runner, NetworkRunnerCallbackArgs.ConnectRequest request, byte[] token)
        {
            //throw new NotImplementedException();
        }

        public void OnConnectFailed(NetworkRunner runner, NetAddress remoteAddress, NetConnectFailedReason reason)
        {
            //throw new NotImplementedException();
        }

        public void OnUserSimulationMessage(NetworkRunner runner, SimulationMessagePtr message)
        {
            //throw new NotImplementedException();
        }

        public void OnSessionListUpdated(NetworkRunner runner, List<SessionInfo> sessionList)
        {
            //throw new NotImplementedException();
        }

        public void OnCustomAuthenticationResponse(NetworkRunner runner, Dictionary<string, object> data)
        {
            //throw new NotImplementedException();
        }

        public void OnHostMigration(NetworkRunner runner, HostMigrationToken hostMigrationToken)
        {
            //throw new NotImplementedException();
        }

        public void OnReliableDataReceived(NetworkRunner runner, PlayerRef player, ArraySegment<byte> data)
        {
            //throw new NotImplementedException();
        }

        public void OnSceneLoadDone(NetworkRunner runner)
        {
            //throw new NotImplementedException();
        }

        public void OnSceneLoadStart(NetworkRunner runner)
        {
            //throw new NotImplementedException();
        }
        #endregion

    }
}

