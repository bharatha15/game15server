using Fusion;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Game15Server
{

    

    /// <summary>
    /// Network Input Data - Storing the loacal inputs on the network side.
    /// </summary>
    public struct NetworkInputData : INetworkInput
    {
        /// <summary>
        /// Player rotation enum
        /// </summary>
        /*public enum PlayerRotation
        {
            None,
            Right,
            Left
        }*/

        /// <summary>
        /// Left Joystick 2D values.
        /// </summary>
        public Vector2 Joystick;
        /// <summary>
        /// Player Vector3 Direction 
        /// </summary>
        public Vector3 Direction;
        /// <summary>
        /// Player Jump
        /// </summary>
        public bool Jumping;
        public Vector2 PlayerRotation;

        public void PlayerRotationValue()
        {
            // Debug.Log($"{PlayerRotation}");
        }

    }
}


